(function(window, undefined) {

  var jimLinks = {
    "f85875fb-5e23-4dde-ad82-c034f3989f63" : {
      "Line_2" : [
        "d12245cc-1680-458d-89dd-4f0d7fb22724"
      ],
      "Button_1" : [
        "706e5d8b-c8c2-433e-9d3d-e05be409c360"
      ],
      "Button_4" : [
        "256db694-b915-4c39-b9dc-eba16569bcf8"
      ]
    },
    "d12245cc-1680-458d-89dd-4f0d7fb22724" : {
      "Line_2" : [
        "9d35966f-3a26-47a8-8a44-6c7e1b5154fc"
      ],
      "Button_1" : [
        "d80d9b6c-225b-48eb-a446-fe5f897a522a"
      ]
    },
    "9d35966f-3a26-47a8-8a44-6c7e1b5154fc" : {
      "Hotspot_1" : [
        "d12245cc-1680-458d-89dd-4f0d7fb22724"
      ],
      "Hotspot_2" : [
        "d12245cc-1680-458d-89dd-4f0d7fb22724"
      ],
      "Hotspot_3" : [
        "d12245cc-1680-458d-89dd-4f0d7fb22724"
      ],
      "Hotspot_4" : [
        "d12245cc-1680-458d-89dd-4f0d7fb22724"
      ]
    },
    "d80d9b6c-225b-48eb-a446-fe5f897a522a" : {
    },
    "256db694-b915-4c39-b9dc-eba16569bcf8" : {
      "Line_2" : [
        "f85875fb-5e23-4dde-ad82-c034f3989f63"
      ],
      "Button_1" : [
        "706e5d8b-c8c2-433e-9d3d-e05be409c360"
      ],
      "Button_2" : [
        "f85875fb-5e23-4dde-ad82-c034f3989f63"
      ]
    },
    "28cddb59-65df-407e-aa8b-335704490d49" : {
      "Line_2" : [
        "f85875fb-5e23-4dde-ad82-c034f3989f63"
      ],
      "Button_1" : [
        "706e5d8b-c8c2-433e-9d3d-e05be409c360"
      ],
      "Button_2" : [
        "f85875fb-5e23-4dde-ad82-c034f3989f63"
      ]
    },
    "706e5d8b-c8c2-433e-9d3d-e05be409c360" : {
      "Line_2" : [
        "f85875fb-5e23-4dde-ad82-c034f3989f63"
      ],
      "Button_1" : [
        "706e5d8b-c8c2-433e-9d3d-e05be409c360"
      ],
      "Button_2" : [
        "f85875fb-5e23-4dde-ad82-c034f3989f63"
      ]
    }    
  }

  window.jimLinks = jimLinks;
})(window);